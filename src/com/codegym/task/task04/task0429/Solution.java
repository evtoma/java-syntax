package com.codegym.task.task04.task0429;

/* 
Positive and negative numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader((System.in)));
        String s=entered.readLine();
        String t=entered.readLine();
        String u=entered.readLine();
        String[] m = {s,t,u};
        int n = 0, p = 0;
        for (int i=0; i<=2; i++) {
            if (m[i].contains("-")) {
                n++;
            } else if (!m[i].matches("0")) p++;
        }
        //System.out.println(m[1]);
        System.out.println("Number of negative numbers: " + n);
        System.out.println("Number of positive numbers: " + p);

        }



    }

