package com.codegym.task.task04.task0443;


/* 
A name is a name

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String name = entered.readLine();
        String yyyy = entered.readLine();
        String mm = entered.readLine();
        String dd = entered.readLine();
        System.out.println("My name is "+name+".");
        System.out.println("I was born on "+mm+"/"+dd+"/"+yyyy);
    }
}
