package com.codegym.task.task04.task0434;


/* 
Multiplication table

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        int row=1;
        while (row<=10) {
            System.out.println(row * 1 + " " + row * 2 + " " + row * 3 + " " + row * 4 + " " + row * 5 + " " + row * 6 + " " + row * 7 + " " + row * 8 + " " + row * 9 + " " + row * 10);
            row++;
        }


    }
}
