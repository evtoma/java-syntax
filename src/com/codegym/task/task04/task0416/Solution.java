package com.codegym.task.task04.task0416;

/* 
Crossing the road blindly

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String stime = entered.readLine();
        double t = Double.parseDouble(stime);
        if (t<0.0 || t>=60.0)
            t=t%60;
            //System.out.println("time is divided %60 and is: "+t);
        if (t>=0&&t<3 || t>=5 &&t<8  || t>=10 &&t<13 || t>=15 &&t<18 || t>=20 &&t<23 || t>=25 &&t<28 || t>=30 &&t<33 || t>=35 &&t<38 || t>=40 &&t<43 || t>=45 &&t<48 || t>=50 &&t<53 || t>=55 &&t<58)
           System.out.println("green");
        else if (t>=3&&t<4 || t>=8 &&t<9  || t>=13 &&t<14 || t>=18 &&t<19 || t>=23 &&t<24 || t>=28 &&t<29 || t>=33 &&t<34 || t>=38 &&t<39 || t>=43 &&t<44 || t>=48 &&t<49 || t>=53 &&t<54 || t>=58 &&t<59)
            System.out.println("yellow");
        else
            System.out.println("red");

    }
}