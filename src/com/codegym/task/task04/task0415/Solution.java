package com.codegym.task.task04.task0415;

/* 
Rule of the triangle

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String s = entered.readLine();
        String t = entered.readLine();
        String u = entered.readLine();

        int a = Integer.parseInt(s), b = Integer.parseInt(t), c= Integer.parseInt(u);

        if ((a-(b+c))<0 && (b-(a+c))<0 && (c-(a+b))<0)
            System.out.println("The triangle is possible.");
        else
            System.out.println("The triangle is not possible.");


    }
}