package com.codegym.task.task04.task0413;

/* 
Day of the week

*/

import javax.sound.midi.Soundbank;
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader typed = new BufferedReader(new InputStreamReader(System.in));
        String s = typed.readLine();
        int entered = Integer.parseInt(s);

        if (entered == 1)
            System.out.println("Monday");
        else if (entered == 2)
            System.out.println("Tuesday");
        else if (entered == 3)
            System.out.println("Wednesday");
        else if (entered == 4)
            System.out.println("Thursday");
        else if (entered == 5)
            System.out.println("Friday");
        else if (entered == 6)
            System.out.println("Saturday");
        else if (entered == 7)
            System.out.println("Sunday");
        else
            System.out.println("No such day of the week");
    }
}