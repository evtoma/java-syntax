package com.codegym.task.task04.task0442;


/* 
Adding

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        //read numbers:
        BufferedReader entered = new BufferedReader (new InputStreamReader(System.in));
        //int n = Integer.parseInt(entered.readLine());
        //sum up:
        int temp=0;

        while (true) { //true means indefinite rolling of while
            int n = Integer.parseInt(entered.readLine()); //I put readline here because this way I force while loop to wait for input; otherwise would read only first line and the it will start rolling by itself
            temp = temp + n;
            if (n==-1){ //break condition
                System.out.println(temp);
                break;

            }
        }



    }
}
