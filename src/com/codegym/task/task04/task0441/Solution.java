package com.codegym.task.task04.task0441;


/* 
Somehow average

*/
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        int a = Integer.parseInt(entered.readLine());
        int b = Integer.parseInt(entered.readLine());
        int c = Integer.parseInt(entered.readLine());
        int temp;

        int m[] = new int[3];
        m[0] = a;
        m[1] = b;
        m[2] = c;

        //rising order
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2 - i; j++)
                if (m[j] > m[j + 1]) {
                    // swap arr[j+1] and arr[i]
                    temp = m[j];
                    m[j] = m[j + 1];
                    m[j + 1] = temp;
                }
        }
        System.out.println(m[1]);
    }
    }
