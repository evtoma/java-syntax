package com.codegym.task.task04.task0436;


/* 
Drawing a rectangle

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader (System.in));
        int n= Integer.parseInt(entered.readLine());
        int m = Integer.parseInt(entered.readLine());
        for (int i=1; i<=n; i++) {
            for (int j = 1; j <= m; j++) {
                System.out.print(8);
            }
            System.out.println();
        }


    }
}
