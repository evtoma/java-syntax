package com.codegym.task.task04.task0419;

/* 
Maximum of four numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String s1 = entered.readLine();
        String s2 = entered.readLine();
        String s3 = entered.readLine();
        String s4 = entered.readLine();
        int n1 = Integer.parseInt(s1);
        int n2 = Integer.parseInt(s2);
        int n3 = Integer.parseInt(s3);
        int n4 = Integer.parseInt(s4);
        int n=0, m=0;

        if (n1<n2)
            n = n2;
        else n=n1;
        if (n3<n4)
            m= n4;
        else m=n3;
        if (n<m)
            System.out.println(m);
        else System.out.println(n);

            //System.out.println();
    }
}
