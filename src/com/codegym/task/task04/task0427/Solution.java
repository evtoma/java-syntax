package com.codegym.task.task04.task0427;

/* 
Describing numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered= new BufferedReader(new InputStreamReader(System.in));
        String s = entered.readLine();
        int nr = Integer.parseInt(s);
        if (nr>=1 && nr<=999)//the number to be in between 1 and 999
        {
            if (nr % 2 == 0 && s.length() == 1) //check if it's even and has length of 1
                System.out.println("even single-digit number"); //if true, print that
            else if (nr % 2 != 0 && s.length() == 1)
                System.out.println("odd single-digit number");
            else if (nr % 2 == 0 && s.length() == 2)
                System.out.println("even two-digit number");
            else if (nr % 2 != 0 && s.length() == 2)
                System.out.println("odd two-digit number");
            else if (nr % 2 == 0 && s.length() == 3)
                System.out.println("even three-digit number");
            else
                System.out.println("odd three-digit number");
        }


    }
}
