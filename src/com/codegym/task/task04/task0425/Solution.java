package com.codegym.task.task04.task0425;

/* 
Target locked!

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String aString = entered.readLine();
        String bString = entered.readLine();
        int a= Integer.parseInt(aString);
        int b = Integer.parseInt(bString);
        if (a>0 && b>0)
            System.out.println(1);
        else if (a<0 && b>0)
            System.out.println(2);
        else if (a<0 && b<0)
            System.out.println(3);
        else if (a>0 && b<0)
            System.out.println(4);
    }
}
