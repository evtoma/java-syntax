package com.codegym.task.task04.task0426;

/* 
Labels and numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String s = entered.readLine();
        int nr = Integer.parseInt(s);
        if (nr>0 && nr%2==0)
            System.out.println("Positive even number");
        else if (nr>0 && nr%2!=0)
            System.out.println("Positive odd number");
        else if (nr == 0)
            System.out.println("Zero");
        else if (nr<0 && nr%2==0)
            System.out.println("Negative even number");
        else
            System.out.println("Negative odd number");

    }
}
