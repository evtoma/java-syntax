package com.codegym.task.task04.task0432;



/* 
You can't have too much of a good thing

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String sir = entered.readLine();// string to be multiplied

        int N = Integer.parseInt(entered.readLine());//the multiplication factor

        int i=0;
        while (i < N){
            System.out.println(sir);
            i++;
        }

    }
}
