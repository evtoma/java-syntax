package com.codegym.task.task04.task0438;

/* 
Drawing lines

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        for (int i=0; i<10;i++) System.out.print(8);
        System.out.println();//this is to avoid starting the next line from the end of previous line, hence giving  wrong length
        for (int j=0; j<10;j++) System.out.println(8);

    }
}
