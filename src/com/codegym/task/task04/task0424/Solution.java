package com.codegym.task.task04.task0424;

/* 
Three numbers

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
        String s1 = entered.readLine(); //enter first number
        String s2 = entered.readLine(); //enter 2nd number
        String s3 = entered.readLine(); //enter the 3rd number
        //parsing the inputs:
        int nr1 = Integer.parseInt(s1);
        int nr2 = Integer.parseInt(s2);
        int nr3 = Integer.parseInt(s3);
        if (s1.matches(s2) && !s3.matches(s2))
            System.out.println(3);
        else if (s2.matches(s3) && !s1.matches(s2))
            System.out.println(1);
        else if (s1.matches(s3) && !s3.matches(s2))
            System.out.println(2);

    }
}
