package com.codegym.task.task04.task0417;

/* 
Do we have a pair?

*/

import java.io.*;

import static java.lang.Integer.parseInt;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));//reading console input
        String s = entered.readLine();
        String t = entered.readLine();
        String u = entered.readLine();


        //three variables:
        int a = parseInt(s), b = parseInt(t), c = parseInt(u);
        if (a==b && b==c) {
            System.out.println(a + " "+b+" "+c);

        }
        else if (a==b || a==c) {
            System.out.println(a +" "+a);

        }
        else if (b==c) {
            System.out.println(b+" "+c);
        }
    }
}