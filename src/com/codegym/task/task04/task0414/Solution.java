package com.codegym.task.task04.task0414;

/* 
Number of days in the year

*/

import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader typed = new BufferedReader(new InputStreamReader(System.in));
        String s = typed.readLine();
        int year = Integer.parseInt(s);
        int leap= 366, reg = 365;

        if (year%400 == 0)
            System.out.println("Number of days in the year: " + leap); //is a leap year
        else if (year%100 == 0)
            System.out.println("Number of days in the year: " + reg); //is a regular year
        else if (year%4 == 0)
            System.out.println("Number of days in the year: " + leap); //is a leap year
        else
            System.out.println("Number of days in the year: " + reg); //is a regular year

    }
}