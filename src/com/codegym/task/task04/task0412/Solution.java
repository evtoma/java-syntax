package com.codegym.task.task04.task0412;

/* 
Positive and negative numbers

*/

import javax.swing.text.html.parser.Parser;
import java.io.*;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        BufferedReader typed = new BufferedReader(new InputStreamReader(System.in));
        String s = typed.readLine();
        int entered = Integer.parseInt(s);

        if (entered>0)
            System.out.println(2*entered);
        else if (entered<0)
            System.out.println(entered+1);
        else
            System.out.println(0);

    }

}