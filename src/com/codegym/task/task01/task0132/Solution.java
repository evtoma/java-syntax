package com.codegym.task.task01.task0132;

/* 
Sum of the digits of a three-digit number

*/

public class Solution {
    public static void main(String[] args) {
        System.out.println(sumDigitsInNumber(546));
    }

    public static int sumDigitsInNumber(int number) {
        //write your code here
        int d1 = number/100;
        int d2 = (number%100)/10;//(number%(10**(string.len(number)-1)))/10**(string.len(number)-2)
        int d3 = number%10;
        return (d1+d2+d3);

    }
}