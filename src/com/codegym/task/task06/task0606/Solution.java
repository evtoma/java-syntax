package com.codegym.task.task06.task0606;

import sun.security.util.Length;

import java.io.*;

/* 
Even and odd digits

*/

public class Solution {

    public static int even;
    public static int odd;

    public static void main(String[] args) throws IOException {
        //write your code here
            int a = 0, b = 0;
            BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
            String s = entered.readLine();
            int numberToProcess = Integer.parseInt(s);
            //test display entered number:
            //System.out.println(numberToProcess);
            //System.out.println("Even: "+ a+ " Odd: " + b);
            int l = s.length();//store the length of the entered string and becomes counter for while loop
            while (l > 0) {
                int growing = numberToProcess / (int) (Math.pow(10, (l - 1)));
                    if (growing % 2 == 0) {
                        even++; a=even;
                    }
                    else{ odd++; b=odd; }
                //System.out.println(numberToProcess / (int)(Math.pow(10,(l-1))));
                l--;

            }
        System.out.println("Even: "+a+ " Odd: "+b);

    }
}
