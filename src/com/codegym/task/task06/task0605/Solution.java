package com.codegym.task.task06.task0605;


import java.io.*;
import java.net.InetSocketAddress;

/* 
Controlling body weight

*/

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
        double weight = Double.parseDouble(bis.readLine());
        double height = Double.parseDouble(bis.readLine());

        Body.calculateBMI(weight, height);
    }

    public static class Body {
        public static void calculateBMI(double weight, double height) { //void, sigur?? poate trebuie returnat ceva.. Sau poate asa e programul initials
            // write your code here

        }

    }
}
