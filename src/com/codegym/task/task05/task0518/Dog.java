package com.codegym.task.task05.task0518;

/*
Dog registration

*/


public class Dog {
    //write your code here
    String name;
    int height;
    String color;
    public Dog(String name1)
    {
        this.name = name1;
    }

    public Dog(String name1, int height1)
    {
        this.name = name1;
        this.height = height1;
    }

    public Dog(String name1, int height1, String color1)
    {
        this.name = name1;
        this.height = height1;
        this.color = color1;
    }

    public static void main(String[] args) {

    }
}
