package com.codegym.task.task05.task0511;

/* 
Create a Dog class

*/

public class Dog {
    //write your code here
    String name;
    int height;
    String color;
    public void initialize(String name1){ //initializing name with its instance variable
        this.name = name1;
    }
    public void initialize(String name1, int height1){ //initializing name and height - in this order - with their instance variables
        this.name = name1;
        this.height = height1;
    }

    public void initialize (String name1, int height1, String color1){ //initializing name, height and color - in this order - with their instance variables
        this.name = name1;
        this.height = height1;
        this.color = color1;
    }

    public static void main(String[] args) {

    }
}
