package com.codegym.task.task05.task0522;

/* 
Max constructors

*/

public class Circle {
    public double x;
    public double y;
    public double radius;

    //write your code here
    public Circle ()
    {
        this.x = 1;
        this.y = 1;
        this.radius = 1;
    }

    public Circle (double x1)
    {
        this.x = x1;
        this.y = 1;
        this.radius = 1;
    }

    public Circle (double x1, double y1)
    {
        this.x = x1;
        this.y = y1;
        this.radius = 1;
    }

    public Circle (double x1, double y1, double radius1)
    {
        this.x = x1;
        this.y = y1;
        this.radius = radius1;
    }

    public static void main(String[] args) {

    }
}