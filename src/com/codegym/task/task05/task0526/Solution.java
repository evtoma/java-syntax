package com.codegym.task.task05.task0526;

/* 
Man and woman

*/

public class Solution {
    public static void main(String[] args) {
        //write your code here
        Man man1 = new Man("John", 37, "Cuba");
        Man man2 = new  Man("Larry", 40, "Jersey");
        Woman wmn1 = new Woman("Vanessa",45, "Paris");
        Woman wmn2 = new Woman("Jena",56,"Berlin");
        System.out.println(man1.name+" "+man1.age+" "+man1.address);
        System.out.println(man2.name+" "+man2.age+" "+man2.address);
        System.out.println(wmn1.name+" "+wmn1.age+" "+wmn1.address);
        System.out.println(wmn2.name+" "+wmn2.age+" "+wmn2.address);



    }

    //write your code here
    public static class Man {
        String name;
        int age;
        String address;
        public Man(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }
    public static class Woman {
        String name;
        int age;
        String address;
        public Woman(String name, int age, String address) {
            this.name = name;
            this.age = age;
            this.address = address;
        }
    }
}
