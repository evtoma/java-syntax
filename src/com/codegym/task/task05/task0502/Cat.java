package com.codegym.task.task05.task0502;

/* 
Implement the fight method

*/

public class Cat {
    public int age;
    public int weight;
    public int strength;

    public Cat() {
    }

    public boolean fight(Cat anotherCat) {
        //write your code here
        boolean winner = false;
        if ((this.age + this.weight + this.strength) >= (anotherCat.age + anotherCat.weight + anotherCat.strength))            winner = true;

        else winner = false;
        return winner;

    }


    public static void main(String[] args) {

        Cat cat1 = new Cat();
        cat1.age = 2;
        cat1.weight = 5;
        cat1.strength = 8;

        Cat cat2 = new Cat();
        cat2.age = 2;
        cat2.weight = 5;
        cat2.strength = 9;

        System.out.println(cat1.fight(cat2));
        System.out.println(cat2.fight(cat1));


    }
}
