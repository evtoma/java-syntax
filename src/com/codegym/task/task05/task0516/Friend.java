package com.codegym.task.task05.task0516;

/* 
You can't buy friends

*/

public class Friend {
    //write your code here
    String name;
    int age;
    char sex;

    public Friend (String name1){
        this.name = name1;
    }

    public Friend (String name1, int age1)
    {
        this. name = name1;
        this. age = age1;
    }

    public Friend (String name1, int age1, char sex1)
    {
        this.name = name1;
        this.age = age1;
        this.sex = sex1;
    }

    public static void main(String[] args) {

    }
}
