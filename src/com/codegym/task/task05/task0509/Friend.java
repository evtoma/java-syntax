package com.codegym.task.task05.task0509;

/* 
Create a Friend class

*/

public class Friend {
    //write your code here
    String name;
    int age;
    char sex;
    public void initialize (String name){
        this.name = name;
    }
    public void initialize (String name, int age){
        this.age = age;
        this.name = name;
    }
    public void initialize (String name, int age, char sex){
        this.sex = sex;
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) {

    }
}
