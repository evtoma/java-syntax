package com.codegym.task.task05.task0513;

/* 
Let's put together a rectangle
Let's put together a rectangle
Create a Rectangle class. The data for this class will be top, left, width, height.
Create as many initialize(...) methods as possible

Here are some examples:
-	4 parameters are specified: left, top, width, and height
-	width/height is not specified (both are 0)
-	height is not specified (it is equal to the width), we'll create a square
-	create a copy of another rectangle passed as an argument


Requirements:
1. The program must not read data from the keyboard.
2. The Rectangle class must have int variables top, left, width and height.
3. The class must have at least one initialize method.
4. The class must have at least two initialize methods.
5. The class must have at least three initialize methods.
6. The class must have at least four initialize methods.
*/

public class Rectangle {
    //write your code here
    //2. The Rectangle class must have int variables top, left, width and height.
    int top, left, width, height;

    //3. The class must have at least one initialize method.
    public void initialize(int top1){
        this.top = top1;
        this.left = 1;
        this.width = 1;
        this.height =1;
    }

    //4. The class must have at least two initialize methods.
    public void initialize(int top1, int left1){
        this.top = top1;
        this.left = left1;
        this.width = 1;
        this.height =1;
    }

    //5. The class must have at least three initialize methods.
    public void initialize(int top1, int left1, int width1){
        this.top = top1;
        this.left = left1;
        this.width = width1;
        this.height =1;
    }

    //6. The class must have at least four initialize methods.
    public void initialize(int top1, int left1, int width1, int height1){
        this.top = top1;
        this.left = left1;
        this.width = width1;
        this.height =height1;
    }

    public static void main(String[] args) {

    }
}
