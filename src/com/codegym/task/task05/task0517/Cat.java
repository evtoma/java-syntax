package com.codegym.task.task05.task0517;

/* 
Creating cats

*/

public class Cat {
    //write your code here
    
    String name;
    int age;
    int weight;
    String address;
    String color;
    
    public Cat(String name1)
    {
        this.name = name1;
        this. age = 1;
        this. weight = 2;
        this. color = "white";
        
    }
    
    public Cat(String name1, int weight1, int age1)
    {
        this.name = name1;
        this.weight = weight1;
        this.age = age1;
        this. color = "white";
    }
    
    public Cat(String name1, int age1)
    {
        this. name = name1;
        this.age = age1;
        this.weight = 2;
        this.color = "white";
    }
    
    public Cat(int weight1, String color1)
    {
        this.age = 2;
        this.weight = weight1;
        this.color = color1;
        
    }
    
    public Cat(int weight1, String color1, String address1)
    {
        this.age = 2;
        this.weight = weight1;
        this.color = color1;
        this.address = address1;
    }
    


    public static void main(String[] args) {

    }
}
