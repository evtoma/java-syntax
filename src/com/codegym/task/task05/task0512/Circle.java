package com.codegym.task.task05.task0512;

/* 
Create a Circle class
Create a Circle class
Create the Circle class with three initializers:
- centerX, centerY, radius
- centerX, centerY, radius, width
- centerX, centerY, radius, width, color


Requirements:
1. The program must not read data from the keyboard.
2. The Circle class must have int variables centerX, centerY, radius, width and color.
3. The class must have an initialize method that takes centerX, centerY, and radius as arguments, and initializes the corresponding instance variables.
4. The class must have an initialize method that takes centerX, centerY, radius, and width as arguments, and initializes the corresponding instance variables.
5. The class must have an initialize method that takes centerX, centerY, radius, width, and color as arguments, and initializes the corresponding instance variables.

*/

public class Circle {
    //write your code here
    //2. The Circle class must have int variables centerX, centerY, radius, width and color.
    int centerX, centerY, radius, width, color;

    //3. The class must have an initialize method that takes centerX, centerY, and radius as arguments, and initializes the corresponding instance variables.
    public void initialize (int centerX1, int centerY1, int radius1){ //initialize the center of the circle and its radius with instance variables
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
    }

    //4. The class must have an initialize method that takes centerX, centerY, radius, and width as arguments, and initializes the corresponding instance variables.
    public void initialize (int centerX1, int centerY1, int radius1, int width1){
        //idem as above plus width
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
        this.width = width1;
    }

    //5. The class must have an initialize method that takes centerX, centerY, radius, width, and color as arguments, and initializes the corresponding instance variables.
    public void initialize (int centerX1, int centerY1, int radius1, int width1, int color1){
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
        this.width = width1;
        this.color = color1;
    }



    public static void main(String[] args) {

    }
}
