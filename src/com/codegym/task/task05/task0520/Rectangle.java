package com.codegym.task.task05.task0520;

/* 
Create a Rectangle class

*/


public class Rectangle {
    //write your code here
    int top, left, width, height;
    public  Rectangle(int top1)
    {
        this.top = top1;
    }
    public  Rectangle(int top1, int left1)
    {
        this.top = top1;
        this.left = left1;
    }
    public  Rectangle(int top1, int left1, int width1)
    {
        this.top = top1;
        this.left = left1;
        this.width = width1;
    }
    public  Rectangle(int top1, int left1, int width1, int height1)
    {
        this.top = top1;
        this.left = left1;
        this.width = width1;
        this.height = height1;
    }

    public static void main(String[] args) {

    }
}
