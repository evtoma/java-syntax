package com.codegym.task.task05.task0507;

/* 
Arithmetic mean

*/

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws Exception {
        //write your code here
        double temp=1, i=0, avg; //initialize temporary buffer temp and counter i with zero; counter i will help in the average formula;
        while (true) {
            BufferedReader entered = new BufferedReader(new InputStreamReader(System.in));
            double num = Integer.parseInt(entered.readLine());
            temp=temp+num;
            i++;
            if (num==-1) { //break condition
                avg = (temp)/(i-1);
                System.out.println(avg);
                ///* for debug control, to delete */System.out.println(i-1);
                break;

            }


        }



    }


}


