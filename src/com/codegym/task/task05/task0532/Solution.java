package com.codegym.task.task05.task0532;

import java.io.*;

/* 
Task about algorithms

*/

public class Solution {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int N = Math.abs( Integer.parseInt( reader.readLine()));
        int maximum=Integer.MIN_VALUE;
        //write your code here
        if (N>0) {
            for (int i= 1;i <= N;i++) {
                int num = Integer.parseInt( reader.readLine());
                maximum=maximum>=num?maximum:num;
            }
        }


        System.out.println(maximum);
    }
}
