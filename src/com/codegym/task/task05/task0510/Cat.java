package com.codegym.task.task05.task0510;

/* 
Initializing cats

*/

public class Cat {
    //write your code here
    String name;
    int age;
    int weight;
    String address;
    String color;

    public void initialize (String name){
        this. name  = name ;
        this. age  = 1 ;
        this. weight  = 1 ;
        this. color  = "white" ;
    }
    public void initialize (String name, int weight, int age){
        this. age  = age ;
        this. name  = name ;
        this. weight  = weight ;
        this. color  = "white" ;
    }
    public void initialize (String name, int age){
        this. age  = age ;
        this. name  = name ;
        this. weight  = 1 ;
        this. color  = "white" ;
    }
    public void initialize (int weight, String color){
        this. age  = 1 ;
        this. weight  = weight ;
        this. color  = color ;
    }
    public void initialize (int weight, String color, String address){
        this. color  = color ;
        this. weight  = weight ;
        this.address=address;
        this.age=1;

    }


    public static void main(String[] args) {

    }
}
