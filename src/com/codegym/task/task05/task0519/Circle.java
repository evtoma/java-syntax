package com.codegym.task.task05.task0519;

/* 
Walking in circles

*/


public class Circle {
    //write your code here
    int centerX, centerY, radius, width, color;
    public Circle(int centerX1, int centerY1, int radius1)
    {
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
    }

    public Circle (int centerX1, int centerY1, int radius1, int width1)
    {
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
        this.width = width1;
    }

    public Circle (int centerX1, int centerY1, int radius1, int width1, int color1)
    {
        this.centerX = centerX1;
        this.centerY = centerY1;
        this.radius = radius1;
        this.width = width1;
        this.color = color1;
    }


    public static void main(String[] args) {

    }
}
